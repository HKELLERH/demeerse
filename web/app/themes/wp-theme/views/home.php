<?php
defined('BASE_PATH') OR exit('No direct script access allowed');
?>

<?php echo $this->view('includes/header');  ?>


<div class="gridd">
	<div class="roww">
		
		<div class="mansory-container">
    	<div class="row mansory-set">
        	<div class="item big">
        		<img src="<?php echo bloginfo('stylesheet_directory'); ?>/assets/build/img/alex-klaasen.jpg" alt="">
        		<div class="text">
            		<time class="date">8 aug 2018</time>
            		<h2 class="h2">Alex Klaasen</h2>
            		<p class="event">Showponies</p>
        		</div>
        	</div>
        	<div class="item small">
        		<img src="<?php echo bloginfo('stylesheet_directory'); ?>/assets/build/img/pieter-derks.jpg" alt="">
        		<div class="text">
            		<time class="date">8 aug 2018</time>
            		<h2 class="h2">Pieter derks</h2>
            		<p class="event">Nu of nooit</p>
        		</div>
        	</div>
        	<div class="item middle">
        		<img src="<?php echo bloginfo('stylesheet_directory'); ?>/assets/build/img/diggy-dex.jpg" alt="">
        		<div class="text">
            		<time class="date">8 aug 2018</time>
            		<h2 class="h2">Diggy Dex</h2>
        		</div>
        	</div>	
        	<div class="item middle middle-2">
        		<img src="<?php echo bloginfo('stylesheet_directory'); ?>/assets/build/img/billy-combinaties.jpg" alt="">
        		<div class="text">
            		<time class="date caps">24 jun tm 12 nov 2018</time>
            		<h2 class="h2">Billy Combinaties</h2>
        		</div>
        	</div>
        	<div class="item small small-2">
        		<img src="<?php echo bloginfo('stylesheet_directory'); ?>/assets/build/img/jandino-asporaat.jpg" alt="">
        		<div class="text">
            		<time class="date">24 jun tm 12 nov 2018</time>
            		<h2 class="h2">Jandino Asporaat</h2>
            		<p class="event">Keihard (reprise)</p>
        		</div>
        	</div>	
    	</div>
    	<div class="row mansory-set right">
        	<div class="item big big-r">
        		<img src="<?php echo bloginfo('stylesheet_directory'); ?>/assets/build/img/alex-klaasen.jpg" alt="">
        		<div class="text">
        			<label></label>
            		<time class="date">8 aug 2018</time>
            		<h2 class="h2">Alex Klaasen</h2>
            		<p class="event">Showponies</p>
        		</div>
        	</div>
        	<div class="item small small-r1">
        		<img src="<?php echo bloginfo('stylesheet_directory'); ?>/assets/build/img/pieter-derks.jpg" alt="">
        		<div class="text">
            		<time class="date">8 aug 2018</time>
            		<h2 class="h2">Pieter derks</h2>
            		<p class="event">Nu of nooit</p>
        		</div>
        	</div>
        	<div class="item middle">
        		<img src="<?php echo bloginfo('stylesheet_directory'); ?>/assets/build/img/diggy-dex.jpg" alt="">
        		<div class="text">
            		<time class="date">8 aug 2018</time>
            		<h2 class="h2">Diggy Dex</h2>
        		</div>
        	</div>	
        	<div class="item middle middle-2 middle-r">
        		<img src="<?php echo bloginfo('stylesheet_directory'); ?>/assets/build/img/billy-combinaties.jpg" alt="">
        		<div class="text">
            		<time class="date caps">24 jun tm 12 nov 2018</time>
            		<h2 class="h2">Billy Combinaties</h2>
        		</div>
        	</div>
        	<div class="item small small-2 small-r2">
        		<img src="<?php echo bloginfo('stylesheet_directory'); ?>/assets/build/img/jandino-asporaat.jpg" alt="">
        		<div class="text">
            		<time class="date">24 jun tm 12 nov 2018</time>
            		<h2 class="h2">Jandino Asporaat</h2>
            		<p class="event">Keihard (reprise)</p>
        		</div>
        	</div>	
    	</div>

	</div>
</div>





<?php echo $this->view('includes/footer');  ?>