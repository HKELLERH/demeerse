<?php
defined('BASE_PATH') OR exit('No direct script access allowed');
?>
<nav class="main-menu">
	<div class="gridd">
		<div class="roww main-menu-holder">
        	<ul>
        		<li>
        			<h2 class="menu-h2">Agenda</h2>
            		<ul>
                		<li><a href="">Cabaret</a></li>
                		<li><a href="">Dans</a></li>
                		<li><a href="">Film</a></li>
                		<li><a href="">Kinderen & familie</a></li>
                		<li><a href="">Musical & show</a></li>
                		<li><a href="">Theaterconcert</a></li>
                		<li><a href="">Toneel & muziektheater</a></li>
            		</ul>
            	</li>
            	<li>
        			<h2>Je bezoek</h2>
            		<ul>
                		<li><a href="">Veelgestelde vragen</a></li>
                		<li><a href="">Bereikbaarheid & Parkeren</a></li>
                		<li><a href="">Ticketinformatie</a></li>
                		<li><a href="">Openingstijden</a></li>
                		<li><a href="">Rolstoelgebruikers</a></li>
                		<li><a href="">Contact</a></li>
            		</ul>
            	</li>
            	<li>
        			<h2>Beeldende kunst</h2>
            		<ul>
                		<li><a href="">Expositie</a></li>
                		<li><a href="">Expositie archief</a></li>
                		<li><a href="">Kunstbemiddeling</a></li>
                		<li><a href="">Over de galerie</a></li>
            		</ul>
            	</li>
            	<li>
        			<h2>Over ons</h2>
            		<ul>
                		<li><a href="">Vacatures</a></li>
                		<li><a href="">Sponsors</a></li>
                		<li><a href="">Zalenverhuur</a></li>
                		<li><a href="">Technische informatie</a></li>
                		<li><a href="">Nieuws</a></li>
                		<li><a href="">Wordt vriend van de Meerse</a></li>
                		<li><a href="">Huisregels & Voorwaarden</a></li>
            		</ul>
            	</li>
        	</ul>
    	</div>
	</div>
</nav>

