<?php
defined('BASE_PATH') OR exit('No direct script access allowed');
?>
<!DOCTYPE html>
<html <?php language_attributes(); ?>>
<head>
    <meta charset="utf-8">
    <meta name="viewport" content="width=device-width, initial-scale=1, shrink-to-fit=no">
    <link href="https://fonts.googleapis.com/css?family=Noto+Sans|Open+Sans&display=swap" rel="stylesheet">
    <?php wp_head(); ?>
</head>
<body >
	<header class="header">
		<div class="gridd">
			<div class="roww">
				<div class="nav-holder">
                    <div class="logo">
                    	<a href="<?php echo bloginfo('url'); ?>">
                    		<img class="dummy" src="<?php echo bloginfo('stylesheet_directory'); ?>/assets/build/img/logo-demeerse.png" alt="De Meerse">
                    	</a>
                    </div>
                    <div class="search">
                    	<div class="searchfield-holder">
                            	<input id="searchfield" class="searchfield" name="search" value="" placeholder="Wat wil je vinden?">
                            	<span class="line"></span>
                            	<a class="search-submit" href="javascript:void(0);"><svg class="icon" height="22" viewBox="0 0 22 22" width="22" xmlns="http://www.w3.org/2000/svg"><path d="m9.9 2.2c-4.2449 0-7.7 3.4551-7.7 7.7s3.4551 7.7 7.7 7.7 7.7-3.4551 7.7-7.7-3.4551-7.7-7.7-7.7m7.7323933 13.8760634 4.0453067 4.0462366c.4301.4301.4301 1.1253 0 1.5554-.2145.2145-.4961.3223-.7777.3223s-.5632-.1078-.7777-.3223l-4.0452036-4.0461336c-1.6943895 1.3564292-3.8426149 2.1684336-6.1770964 2.1684336-5.4593 0-9.9-4.4407-9.9-9.9s4.4407-9.9 9.9-9.9 9.9 4.4407 9.9 9.9c0 2.3340071-.8116744 4.4818338-2.1676067 6.1760634z"/></svg></a>
                    	</div>
                    	<span class="shadowinput searchfield" style="position:absolute;left:-100%;"></span>
                    </div>
                    <div class="service">
                     		<nav>
                         		<ul>
                            		<li><a class="search-button" href="javascript:void(0);">
                            				<svg class="icon glass" height="22" viewBox="0 0 22 22" width="22" xmlns="http://www.w3.org/2000/svg"><path d="m9.9 2.2c-4.2449 0-7.7 3.4551-7.7 7.7s3.4551 7.7 7.7 7.7 7.7-3.4551 7.7-7.7-3.4551-7.7-7.7-7.7m7.7323933 13.8760634 4.0453067 4.0462366c.4301.4301.4301 1.1253 0 1.5554-.2145.2145-.4961.3223-.7777.3223s-.5632-.1078-.7777-.3223l-4.0452036-4.0461336c-1.6943895 1.3564292-3.8426149 2.1684336-6.1770964 2.1684336-5.4593 0-9.9-4.4407-9.9-9.9s4.4407-9.9 9.9-9.9 9.9 4.4407 9.9 9.9c0 2.3340071-.8116744 4.4818338-2.1676067 6.1760634z"/></svg>
                            				<svg class="icon cross close-search" height="14" viewBox="0 0 14 14" width="14" xmlns="http://www.w3.org/2000/svg"><g  transform="matrix(.70710678 .70710678 -.70710678 .70710678 6.493903 -6.435029)"><rect height="18" rx="1.125" transform="matrix(0 1 -1 0 19 -.715728)" width="2.25" x="8.732864" y=".142136"/><rect height="18" rx="1.125" width="2.25" x="8.732864" y=".142136"/></g></svg>
                            			</a>
                            		</li>
                            		<li><a href=""><svg class="icon"height="22" viewBox="0 0 24 22" width="24" xmlns="http://www.w3.org/2000/svg"><path d="m22 6.1875h-3.4c0-.06875-.0666667-.1375-.0666667-.20625l-3.3333333-5.5c-.2-.275-.4666667-.48125-.8666667-.48125-.5333333 0-1 .48125-1 1.03125 0 .20625.0666667.4125.1333334.55l2.8 4.60625h-8.53333337l2.79999997-4.60625c.0666667-.1375.1333334-.34375.1333334-.55 0-.55-.4666667-1.03125-1.00000003-1.03125-.33333334 0-.66666667.20625-.86666667.48125l-3.33333333 5.5c-.06666667.06875-.06666667.1375-.06666667.20625h-3.4c-1.13333333 0-2 .89375-2 2.0625v1.375c0 .9625.66666667 1.7875 1.53333333 1.99375l2.53333334 8.86875c.26666666.825 1 1.5125 1.93333333 1.5125h12c.9333333 0 1.6666667-.61875 1.9333333-1.5125l2.5333334-8.86875c.8666666-.20625 1.5333333-1.03125 1.5333333-1.99375v-1.375c0-1.16875-.8666667-2.0625-2-2.0625zm-20 2.0625h20v1.375h-20zm16 11.6875h-12l-2.33333333-8.25h16.73333333z"/></svg></a></li>
                            		<li><a href=""><svg class="icon" height="22" viewBox="0 0 20 22" width="20" xmlns="http://www.w3.org/2000/svg"><path d="m18.7 22c-.6072 0-1.1-.4917-1.1-1.1v-2.2c0-1.8194-1.4806-3.3-3.3-3.3h-8.8c-1.8194 0-3.3 1.4806-3.3 3.3v2.2c0 .6083-.4928 1.1-1.1 1.1s-1.1-.4917-1.1-1.1v-2.2c0-3.0327 2.4673-5.5 5.5-5.5h8.8c3.0327 0 5.5 2.4673 5.5 5.5v2.2c0 .6083-.4928 1.1-1.1 1.1zm-8.8-11c-3.0327 0-5.5-2.4673-5.5-5.5s2.4673-5.5 5.5-5.5 5.5 2.4673 5.5 5.5-2.4673 5.5-5.5 5.5zm0-8.8c-1.8194 0-3.3 1.4806-3.3 3.3s1.4806 3.3 3.3 3.3 3.3-1.4806 3.3-3.3-1.4806-3.3-3.3-3.3"/></svg></a></li>
                            		<li>
                            			<a href="javascript:void(0);" class="menu-button">
                            				<span class="menu-txt">MENU</span>
                            				<svg class="icon menu" height="18" viewBox="0 0 18 18" width="18" xmlns="http://www.w3.org/2000/svg"><g><rect height="2.25" rx="1.125" width="18"/><rect height="2.25" rx="1.125" width="18" y="15.75"/><rect height="2.25" rx="1.125" width="18" y="7.875"/></g></svg>
                            				<svg class="icon cross close-menu" height="14" viewBox="0 0 14 14" width="14" xmlns="http://www.w3.org/2000/svg"><g  transform="matrix(.70710678 .70710678 -.70710678 .70710678 6.493903 -6.435029)"><rect height="18" rx="1.125" transform="matrix(0 1 -1 0 19 -.715728)" width="2.25" x="8.732864" y=".142136"/><rect height="18" rx="1.125" width="2.25" x="8.732864" y=".142136"/></g></svg>
                            			</a>
                            		</li>
                        		</ul>
                    		</nav>
                    </div>
                  </div>
			</div>
		</div>
		
	</header>
	<?php echo $this->view('includes/menu');  ?>
	<main>
