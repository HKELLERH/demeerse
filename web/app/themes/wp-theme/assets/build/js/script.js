import $ from 'jquery';

$( document ).ready(function() {
	
	var shadowinput  = $(".shadowinput");
	var line 		 = $(".line");
	var maxlinewidth = 0;
	var previtem	 = 0;
	var curritem	 = 0;

	//search button
	$( ".search-button" ).click(function() {
	  $(this).resetMenu();	
	  var search = $(".search");
	  search.toggleClass('active');
	  search.find('input').focus();
	  search.getlinewidth();
	  $(this).find(".glass").toggle();	
	  $(this).find(".cross").toggle();
	});
	
	//menu button
	$( ".menu-button" ).click(function() {
	  $(this).resetSearch();	  	
	  $(".main-menu").toggle();
	  $(this).find(".menu").toggle();	
	  $(this).find(".cross").toggle();
	});
	
	//mobile menu
	$( ".main-menu h2" ).click(function() {
		//get current item
		curritem = $(this).parent().find("> ul");	
		//toggle current item
		curritem.toggleClass("active");
		//hide prev item if different and exists
		if(previtem && !curritem.is(previtem)){
			previtem.removeClass("active");
		}
		//save current item
		previtem = curritem;
	});
	
	//on resize 
	$(window).resize(function(){
	  //reset search section
	  $(this).resetSearch();	 
	  $(this).resetMenu();	
	  $(".searchfield").val('');
	  line.css('width',"0px");
	});
	
	//search field underline hightligting
	$(".searchfield").bind("keyup keydown", function(){
		shadowinput.text($(this).val());
		var newwidth = shadowinput.outerWidth();
		if(newwidth < maxlinewidth){
			line.css('width',newwidth + "px");
		}
		else{
			line.css('width',maxlinewidth + "px");
		}
	});
	
	//get maxwidth seachfield
	(function( $ ){
	   $.fn.getlinewidth = function() {
		   maxlinewidth = $(".searchfield").css('width');
		   maxlinewidth = maxlinewidth.slice(0, -2);
		   return maxlinewidth;
	   }; 
	})( $ );
	
	//reset menu
	(function( $ ){
	   $.fn.resetMenu = function() {
		   $(".main-menu").hide();	
		   $(".menu").show();	
		   $(".close-menu").hide();	
	   }; 
	})( $ );
	
	//reset search
	(function( $ ){
	   $.fn.resetSearch = function() {
		   $(".search").removeClass('active');	
		   $(".glass").show();	
		   $(".close-search").hide();	
	   }; 
	})( $ );
	
	
});

